#### Notes about this folder, and the code in it:

Code in this folder was copied from the FishBus repository.  Dependencies may be missing, but this is what the design is based on.

message\_queue.lua is the important part.  The RCON-code should call `mqueue.get <system> <index>` in order to get the most recent messages in the queue for that system.

In the case of the keystore code, this means that the RCON caller calls `mqueue.get keystore 0` the first time.  Upon getting new results, that value gets increased to the latest value we've processed.

message\_queue.lua does some pruning of it's data, in order to prevent just storing *everything*.  Defaults are the latest 100 messages per system + the latest 5 minutes (assuming 60-tick/sec game => 18000 ticks).  Both conditions count.  Eg. messages are only purged if there are more than 100, and the message is > 5 minutes old.

So if a single system generates more than 100 messages in 5 minutes, then there will be more than 100 messages in the queue for that system.  If a system generates 1 message per minute, then there will be 100 minutes of messages in queue.

Multiple RCON clients can connect to get queue data.  The code does not care.
