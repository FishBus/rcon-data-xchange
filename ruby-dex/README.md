# FishBus' Ruby-DEX

### Setup

#### Dependencies

* Ruby (v2.4 tested)
* RubyGem's Bundler
* Postgresql (tested with 9.4, 9.6, should be fine with any version)

#### Installation

This is a hopefully standardish ruby software installation.  It expects ruby (2.2+, I think, though I've been using 2.4) to be installed, as well as the gem `bundler`.

Grab as either a git clone, or zipfile, and place where desired.

Create a database and database user in postgresql.  (example below.)
```
> psql -U postgres -d template1
```
```sql
CREATE USER mydexuser WITH PASSWORD 'my-super-secret-password';
CREATE DATABASE dexdata WITH OWNER mydexuser;
```

Create a configuration file in `config/db.yml`.  It should look something like the blurb below.  You should avoid modifying `config/defaults/db.yml` and really create a new one.
```yaml
production:
   host: localhost
   port: 5432
   username: mydexuser
   password: my-super-secret-password
   database: dexdata
```

Enter into the ruby-dex folder, and run:
```
bundler install --path .bundle
```

It will install the dependencies for use with bundler.  The rakefile assumes using bundler, and will run subcommands using it.

Out of habit, I expect a RACK\_ENV variable to be set to 'production' when running in production.  (This comes from using ruby in webapps.  Sorry users!)

This is how it's done in tcsh, bash will have to use `export`
```tcsh
setenv RACK_ENV production
```

Create the database schema:
```
rake migrate
```

#### Running....

You'll want to add a server to the database.  Currently this is done manually.  You can easily use psql to connect to the database using this command:
```
rake dbconsole
```

You should create a server group and a server:
```sql
INSERT INTO server_groups (name) VALUES ('My Server Group');
INSERT INTO servers (name, host, server_group_id, rcon_port, rcon_pass) VALUES ('My server', '127.0.0.1', 1, 34297, 'my-rcon-password');
```
(Note: in the above lines, it's assumed that we're creating the first server group in the database.  Thus the 3rd value to `servers` might need to change from 1 to something else if this is not the first server group.)

It's recommended to run DEX in something like tmux, so that you could see what's going on in the console if something goes awry.

Then, just run the program:
```
rake run
```

#### Runtime notes:

Every 15 seconds, DEX will check it's server list to see if you add, remove or disable a server in the database.  So DEX only needs a restart if you need the code to change, such as adding a new 'unit' or changing the way one works (bug fixes, etc).  Server changes need no updates; they will automatically be picked up.

Also note, that since the RCON port will be connected at all times, that you will probably need to wait for the port to time out when restarting your game server.  Factorio needs to be updated to use a particularly flag on the rcon port when it's created so that this delay can be ignored.

#### Converting users from permission\_users.lua

Export data from permission_users.lua:

In a lua console (example is run from inside the scripts folder)
```lua
users = require 'permission_users'
json = require 'lib/dkjson'
print(json.encode(users))
```

Copy that json output a ruby console (in ruby-dex).  Adjust sg\_id to reference the appropriate server group's ID, or change the statements to reference a single server.  Easiest for startup, is to use the rake command to get into the console, connect to the database, and load the model used.
```
> rake irb
```
```ruby
data = JSON.parse('... data copied from above ...')
sg_id = 1
data.each do |key, value|
  kv = (KeyValue.filter(:module => 'perm.player', :key => key, :server_group_id => sg_id).first or KeyValue.new)
  if kv.new?
    kv.module = 'perm.player'
    kv.key = key
    kv.server_group_id = sg_id
  end
  kv.value = value
  kv.save
end
```
