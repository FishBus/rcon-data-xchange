class MQueueUnit
	def initialize(server)
		@server = server
	end
	
	def last_tick
		@server.server.last_tick
	end
	
	def query_and_process
		# override me, this is called on every iteration!
	end
	
	def restart
		# Called when a map has a major change.
	end
end
