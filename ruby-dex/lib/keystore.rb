# Copyright 2018 "Kovus" <kovus@soulless.wtf>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# keystore.rb
#
# A Key-Value external storage mechanism for Factorio mods.
# Able to share data between servers as well.

require "#{__dir__}/unit_manager"
require "#{__dir__}/mqueue_unit"

class Keystore < MQueueUnit
	def initialize(server)
		super(server)
		puts "Initializing keystore for #{server.server.id}: #{server}"
		@lastindex = 0
	end
	
	def self.name
		return "keystore"
	end
	
	def processEntry(jentry)
		puts "Processing entry #{jentry}"
		res = {}
		if jentry['index'].to_i > @lastindex
			case jentry['message']['op']
			when "store" then
				res.merge!(store(jentry['message']))
			when "retrieve" then
				res.merge!(retrieve(jentry['message']))
			else
				puts "DEBUG: Unknown entry processing"
				res[:error] = "Unknown operation"
			end
		end
		res
	end
	
	def query_and_process
		#puts "DEBUG: Keystore#query_and_process"
		@server.rcon.command("mqueue.get #{Keystore.name} #{@lastindex}") do |packet|
			puts "DEBUG: Keystore#query_and_process" # #{packet.packet.to_s[12..-1].strip}"
			highest_index = 0
			highest_tick = 0
			res = packet.packet.to_s[12..-1].strip.split("\n")
			res.each do |rr|
				begin
					jres = JSON.parse(rr)
					if jres['count'].to_i > 0 and jres['entries'].class == Array
						jres['entries'].each do |entry|
							#puts "Evaluating entry #{entry} (lasttick: #{last_tick}) for processing"
							highest_index = [highest_index, entry['index'].to_i].max
							highest_tick = [highest_tick, entry['tick'].to_i].max
							next if entry['tick'].to_i < last_tick
							res = processEntry(entry)
							res[:callback_id] = entry['message']['callback_id'].to_i
							send_response(res)
						end
					end
					puts jres
				rescue JSON::ParserError
					puts "DEBUG: #query_and_process: Unable to parse #{rr}"
				end
			end
			#puts "DEBUG: Lastindex [#{@lastindex}, #{highest_index}].max ..."
			@lastindex = [@lastindex, highest_index].max
		end
	end
	
	def restart
		@lastindex = 0
	end
	
	def retrieve(data)
		ds = KeyValue.where(:module => data['module'], :key => data['field'])
		kv = ds.filter(:server_id => @server.server.id).first
		if not kv
			kv = ds.filter(:server_group_id => @server.server.server_group_id).first
		end
		if not kv
			return {module: data['module'], field: data['field'], :error => "key not found"}
		end
		return {
			module: kv.module,
			field: kv.key,
			value: JSON.parse(kv.value),
			status: "success",
		}
	end
	
	def send_response(res)
		puts "DEBUG: #send_response(#{res})"
		if res[:callback_id].to_i > 0
			@server.rcon.message("/keystore.event #{res.to_json}")
		end
	end
	
	def store(data)
		ds = KeyValue.where(:module => data['module'], :key => data['field'])
		if data['global'] and [true, 'true'].include? data['global']
			ds = ds.filter(:server_group_id => @server.server.server_group_id)
		else
			ds = ds.filter(:server_id => @server.server.id)
		end
		kv = ds.first
		if not kv
			kv = KeyValue.new
			kv.module = data['module']
			kv.key = data['field']
		end
		if data['global'] and [true, 'true'].include? data['global']
			kv.server_group_id = @server.server.server_group_id
			kv.server_id = nil
		else
			kv.server_group_id = nil
			kv.server_id = @server.server.id
		end
		kv.value = data['value'].to_json
		kv.new? ? kv.save : kv.save_changes
		return {:status => "success"}
	end
end

$unitManager.register(Keystore)
