require 'eventmachine'
require 'ripl'

class FBDexKeyboardHandler < EM::Connection
	include EM::Protocols::LineText2

	def initialize(q, servers)
		@queue = q
		@servers = servers
	end

	def receive_line(data)
		#p data
		#@queue.push(data)
		if data.strip == "quit" || data.strip == "q"
			@servers.each do |idx, serv|
				p serv.server
				serv.server.save_changes
			end
			EM.stop
		end
		if data.strip == "irb"
			Ripl.start :binding => binding
		end
		if data.strip == "list" || data.strip == "ls"
			@servers.each do |idx, serv|
				p serv.server
			end
		end
		if data.strip == "rcon"
			idx = data.scan(/\s(\d+)\s/)[0][0]
			if @servers[0] and @servers[0].rcon
				@servers[0].rcon.command(data) do |packet|
					puts "Actual output: "
					puts packet.packet.to_s[0..11] # junk?
					res = packet.packet.to_s[12..-1].strip.split("\n")
					puts res
					res.each do |rr|
						begin
							jres = JSON.parse(rr)
							puts jres
						rescue
							puts "DEBUG: Unable to parse #{rr}"
						end
					end
				end
			end
		end
	end
	
end

