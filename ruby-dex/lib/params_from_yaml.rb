require 'yaml'

# found on https://stackoverflow.com/questions/800122/best-way-to-convert-strings-to-symbols-in-hash
class Object
	def deep_symbolize_keys
		return self.inject({}){|memo,(k,v)| memo[k.to_sym] = v.deep_symbolize_keys; memo} if self.is_a? Hash
		return self.inject([]){|memo,v    | memo           << v.deep_symbolize_keys; memo} if self.is_a? Array
		return self
	end
end

module Helper
	class Config
		def self.hash_from_yaml config, defaultconfig=nil
			params = {}
			env = ENV['RACK_ENV'] || ENV['STORE_ENV'] || "development"
			if defaultconfig
				if File.exists? defaultconfig
					YAML::load_file("#{defaultconfig}")[env].each do |key, value|
						params[key] = value
					end
				else
					print "WARNING: Unable to locate default config file #{defaultconfig}\n" unless env == "test"
				end
			end
			if config and File.exists? config
				YAML::load_file("#{config}")[env].each do |key, value|
					params[key] = value
				end
			else
				print "WARNING: Unable to locate config file, #{config}\n" unless env == "test"
			end
			return params.deep_symbolize_keys
		end
	end
end

