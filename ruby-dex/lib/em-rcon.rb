# Copyright 2018 "Kovus" <kovus@soulless.wtf>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# em-rcon.lua
#
# EventMachine-based Source-style RCON interface for Factorio.
#
# Designed largely for fire & get small response queries.
#
# Callback design in here is built so that a single command will get a single
# response.  It is not designed to call every callback on every response.

module RCon
	module Packet
		class Source
			COMMAND_EXEC = 2	# execution command
			COMMAND_AUTH = 3	# auth command
			RESPONSE_AUTH = 2	# auth response
			RESPONSE_NORM = 0	# normal response
	
			TRAILER = "\x00\x00"	# packet trailer
	
			# size of the packet (10 bytes for header + string1 length)
			attr_accessor :packet_size
			# Request Identifier, used in managing multiple requests at once
			attr_accessor :request_id
			# Type of command/reponse.  See COMMAND_* & RESPONSE_* above.
			attr_accessor :command_type
			# First string, the only used one in the protocol, contains
			# commands and responses. Null terminated.
			attr_accessor :string1
			attr_accessor :string2 # unused in protocol, but expected
			
			def initialize()
				@request_id = rand(2**32-1)
				@command_type = RESPONSE_NORM
				@string1 = ''
				@string2 = TRAILER
			end
			
			#
			# Generate an authentication packet to be sent to a newly
			# started RCon connection. Takes the RCon password as an
			# argument.
			#
			def auth(string)
				@request_id = rand(2**32-1)
				@command_type = COMMAND_AUTH
				@string1 = string
				@packet_size = build_packet.length
				return self
			end
			
			#
			# Generate a command packet to be sent to an already
			# authenticated RCon connection. Takes the command as an
			# argument.
			# 
			def command(string)
				@request_id = rand(2**32-1)
				@command_type = COMMAND_EXEC
				@string1 = string
				@packet_size = build_packet.length
				return self
			end
	
			#
			# Builds a packet ready to deliver, without the size prepended.
			# Used to calculate the packet size, use #to_s to get the packet
			# that srcds actually needs.
			#
			def build_packet
				return [@request_id, @command_type, @string1, @string2].pack("VVa#{@string1.length}a2")
			end
			
			def isAuthResponse?
				return @command_type == RESPONSE_AUTH
			end
			def isGoodAuth?
				return (@command_type == RESPONSE_AUTH and @request_id != 0xFFFFFFFF)
			end
			
			# Returns a string representation of the packet, useful for
			# sending and debugging. This include the packet size.
			def packet
				packet = build_packet
				# size of the packet (10 bytes for header + string1 length)
				@packet_size = packet.length
				return [@packet_size].pack("V") + packet
			end
			
			#
			# Take the response (or any string value) and build a 
			# RCon::Source::Packet from it.
			# assumes that the size has already been received.
			#
			def self.parse_raw(size, string)
				#print "DEBUG: Parsing: #{string}\n"
				packet = self.new
				packet.packet_size = size
				psize, packet.request_id, packet.command_type, packet.string1, packet.string2 = string.unpack("V1V1V1a*a*")
				# strip nulls from end of strings.
				packet.string1.sub! /\x00\x00$/, ""
				packet.string2.sub! /\x00\x00$/, ""
				return packet
			end
		end
	end
end


class EMRcon < EM::Connection
	attr_reader :ready
	
	def initialize(pass)
		print "DEBUG: Initialize EMRCon.\n"
		@pass = pass
		@authed = false
		@ready = false
		@cb_queue = []
		@data_queue = []
	end
	
	def authenticate(&cb)
		print "DEBUG: auth\n"
		packet = RCon::Packet::Source.new
		packet.auth(@pass)
		puts "DEBUG: (auth) Sending #{packet.packet.to_s}"
		send_data packet.packet.to_s
		#blank = RCon::Packet::Source.new
		#send_data blank.packet
		@cb_queue.unshift(cb)
		#print "DEBUG: Queue: #{@cb_queue}\n"
	end
	
	def check_auth(result)
		puts "DEBUG: Auth-response: #{result.request_id} #{result.command_type}"
		if result.isAuthResponse?
			if result.isGoodAuth?
				@authed = true
			else
				puts "DEBUG: FAILED AUTHENTICATION."
			end
		end
	end
	
	def command(command, &cb)
		#print "DEBUG: command execution.\n"
		if not @authed
			authenticate() { |res|
				check_auth(res)
				puts "DEBUG: post-auth, Send_command(#{command})"
				send_rcon_cmd(command) {|cb| yield(cb)}
			}
		else
			puts "DEBUG: Send_command(#{command})"
			send_rcon_cmd(command) {|cb| yield(cb)}
		end
	end
	
	def connection_completed
		print "DEBUG: Connected.\n"
		@ready = true
		authenticate() { |res| 
			check_auth(res)
		}
	end
	
	def message(message)
		#print "DEBUG: message execution.\n"
		if not @authed
			authenticate() { |res|
				check_auth(res)
				puts "DEBUG: post-auth, Send_message(#{message})"
				send_rcon_msg(message)
			}
		else
			puts "DEBUG: Send_message(#{message})"
			send_rcon_msg(message)
		end
	end
	
	def post_init
		print "DEBUG: post_init.\n"
		#authenticate() {|res| 
		#	command("help") {|res|
		#		puts "DEBUG: help res: #{res}"
		#	}
		#}
	end
	
	def receive_data(data)
		# Since we always send a blank command at the end, we can expect a
		# "blank" response after the full message.  This lets us know when
		# we have finished receiving data, and are ready to pass it on to
		# the callback.
		@data_queue << data
		find_and_handle_packet()
	end
	
	def send_rcon_cmd(command, &cb)
		#print "DEBUG: send_command: #{command}\n"
		# send command.
		packet = RCon::Packet::Source.new
		# check if the command already has a leading slash.
		if command[0] == '/'
			packet.command(command)
		else
			packet.command("/"+command) # add leading slash.
		end
		send_data packet.packet
		@cb_queue.unshift(cb)
		#print "DEBUG: Queue: #{@cb_queue}\n"
	end
	
	def send_rcon_msg(message)
		#print "DEBUG: send_message: #{message}\n"
		# send message.
		packet = RCon::Packet::Source.new
		# messages look just like commands, but there's no:
		# - leading slash; nor
		# - response handling. (fire & forget)
		packet.command(message)
		send_data packet.packet
		#print "DEBUG: Queue: #{@cb_queue}\n"
	end
	
	def find_and_handle_packet()
		if @data_queue.size == 0
			return
		end
		size = @data_queue[0].unpack("V1")[0]
		queuesize = @data_queue.map{|x| x.size}.sum
		#puts "\t Searching for size #{size+4} in #{queuesize}"
		if size+4 <= queuesize
			# get the first packet off the queue.
			remain = size+4
			str = ""
			consumed = 0
			#puts "DEBUG: Queued Data: #{@data_queue}"
			@data_queue = @data_queue.map! do |data|
				if remain > 0
					str += data[0..(remain-1)]
				end
				if data.size <= remain
					consumed += 1
					remain -= data.size
				else
					data = data[remain..-1]
					remain -= data.size
				end
				data
			end
			#puts "Comsumed #{consumed} packet(s)"
			@data_queue = @data_queue.drop(consumed)
			#print "DEBUG: #receive_data: (#{size}, #{str.size}) #{str}\n"
			packet = RCon::Packet::Source.parse_raw(size, str)
			#print "DEBUG: Recv: #{packet.packet.to_s[12..-1]}\n"
			#print "DEBUG: #receive_data: cb queue: (#{@cb_queue.size})\n"
			cb = @cb_queue.pop
			if cb
				cb.call(packet)
			end
			find_and_handle_packet()
		end
	end
	
	def unbind
		print "DEBUG: Unbound EMRcon connection\n"
		@ready = false
		@authed = false
		@cb_queue.clear
	end
	
end

