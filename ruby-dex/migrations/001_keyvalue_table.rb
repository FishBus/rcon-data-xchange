Class.new(Sequel::Migration) do
	def up
		print "UP Migration: #{__FILE__}\n"
		
		create_table :key_values do
			primary_key	:id
			String      :module, :size => 100, :null => false
			String      :key, :size => 255, :null => false
			String      :value
			index       [:module, :key], :unique => true
		end
	end
	
	def down
		print "DOWN Migration: #{__FILE__}\n"
		drop_table :key_values
	end
end
