Class.new(Sequel::Migration) do
	def up
		print "UP Migration: #{__FILE__}\n"
		
		create_table :server_groups do
			primary_key	:id
			String      :name, :size => 100, :null => false, :unique => true
		end
		create_table :servers do
			primary_key	:id
			String      :name, :size => 100, :null => false
			Boolean     :enabled, :default => true
			foreign_key :server_group_id, :server_groups, :on_delete => :set_null
			String      :host, :size => 100, :null => false
			Integer     :rcon_port, :null => false
			String      :rcon_pass, :size => 100, :null => false
			index       [:host, :rcon_port], :unique => true
			Integer     :seed
			Integer     :last_tick
		end
	end
	
	def down
		print "DOWN Migration: #{__FILE__}\n"
		drop_table :servers
		drop_table :server_groups
	end
end
