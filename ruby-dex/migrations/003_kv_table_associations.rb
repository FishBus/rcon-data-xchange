Class.new(Sequel::Migration) do
	def up
		print "UP Migration: #{__FILE__}\n"
		
		alter_table :key_values do
			add_foreign_key :server_group_id, :server_groups, :on_delete => :cascade
			add_foreign_key :server_id, :servers, :on_delete => :cascade
			drop_index      [:module, :key]
			add_index       [:module, :key, :server_group_id, :server_id], :unique => true, :name => "key_values_unique_key_per_server"
		end
	end
	
	def down
		print "DOWN Migration: #{__FILE__}\n"
		alter_table :key_values do
			drop_foreign_key :server_group_id
			drop_foreign_key :server_id
			#drop_index       [:module, :key, :server_group_id, :server_id], :unique => true, :name => "key_values_unique_key_per_server"
			add_index        [:module, :key], :unique => true
		end
		# for some reason, this isn't being dropped correctly by ruby-sequel:
		DB["DROP INDEX key_values_unique_key_per_server"]
	end
end
