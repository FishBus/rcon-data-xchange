# Copyright 2018 "Kovus" <kovus@soulless.wtf>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# fbdex.rb
#
# FishBus Data EXchange system.
# Originally meant to be able to store & save persistent data between Factorio
# servers, it quickly became apparent that with a few small tweaks, this code
# could be generalized into something that can handle not just storage of 
# data, but exchange of data between game servers, as well as execution of 
# commands to game servers (for example, banning a user)
# 
# Use EventMachine to watch some files for requests, and then respond on those
# requests via rcon back to the appropriate factorio rcon port.

require 'eventmachine'
require "#{__dir__}/lib/em-rcon"
require "#{__dir__}/lib/params_from_yaml"
require 'json'
require 'yaml'

# DB model
require "#{__dir__}/model/init"

# modules that define what operations we handle:
require "#{__dir__}/lib/fbdex_console"
require "#{__dir__}/lib/init_units"

settings = Helper::Config.hash_from_yaml("#{__dir__}/config/defaults/servers.yml")

# scan through the config file server list, add them to the DB if necessary.
settings[:servers].each do |ss|
	p ss
	# "connect" rcon ?
	exist = Server.filter(:host => ss[:ip], :rcon_port => ss[:rcon_port]).first
	if not exist
		serv = Server.new
		serv.name = "Unnamed"
		serv.host = ss[:ip]
		serv.rcon_port = ss[:rcon_port]
		serv.rcon_pass = ss[:rcon_pass]
		serv.save
	end
end

# for efficient file watching, use kqueue on Mac OS X
EM.kqueue = true if EM.kqueue?

$servers = {}

def em_reload_servers
	#puts Server.all
	Server.all.each do |server|
		#p "check server ##{server.id}\n"
		if not $servers[server.id] and server.enabled
			# connect to server
			puts "INFO: Reconfiguring Server ID #{server.id}"
			rcon = EM::connect server.host, server.rcon_port, EMRcon, server.rcon_pass
			serv = DEXServer.new(server, rcon, server.last_tick)
			
			$servers[server.id] = serv
			$unitManager.units.each do |key, unitClass|
				serv.units.push unitClass.new(serv)
			end
		end
		if $servers[server.id] and server.enabled == false
			$servers[server.id].closeup
			$servers.delete(server.id)
		end
	end
end

if __FILE__ == $0
	EM.run do
		em_reload_servers
		
		q = EM::Queue.new
		EM.open_keyboard(FBDexKeyboardHandler, q, $servers)
		
		EM.add_periodic_timer(15) do
			puts "Periodic event! #{Time.now}"
			em_reload_servers
			$servers.each do |idx, server|
				if not server.rcon or server.rcon.error? or server.rcon.ready == false
					# Disconnect.  Reconnect.
					puts "Reconnecting server ##{server.server.id}"
					server.rcon.close_connection(false)
					server.rcon.reconnect(server.server.host, server.server.rcon_port)
				end
			end
		end # periodic timer
	end
end
