# requires
require 'sequel'

# initialize database.
require "#{__dir__}/../lib/params_from_yaml"

dbparams = Helper::Config.hash_from_yaml("#{__dir__}/../config/db.yml", "#{__dir__}/../config/defaults/db.yml")
p dbparams
DB = Sequel.connect("postgres://#{dbparams[:username]}:#{dbparams[:password]}@#{dbparams[:host]}/#{dbparams[:database]}")

# load some plugins/extensions.
#Sequel::Model.plugin :hook_class_methods
#Sequel.extension :pg_json_ops
#Sequel.extension :no_auto_literal_strings
#DB.extension :pg_array, :pg_json
DB.extension :connection_validator
DB.pool.connection_validation_timeout = 200 # 200 seconds, then check.

@models = [
	'dexserver',
	'keyvalue.rb',
	'server_group.rb',
	'server.rb',
]

@models.each do |model|
	require "#{__dir__}/#{model}"
end

# enable up DB logger.
env = ENV["RACK_ENV"] || ENV["STORE_ENV"] || "development"
if env != 'production'
	require 'logger'
	DB.loggers << Logger.new($stdout)
end
