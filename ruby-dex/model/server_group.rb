class ServerGroup < Sequel::Model
	one_to_many :servers
	one_to_many :key_values
end
