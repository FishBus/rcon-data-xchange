class Server < Sequel::Model
	many_to_one :server_group
	one_to_many :key_values
end
