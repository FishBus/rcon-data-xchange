# This may be a bad location for this object definition, as it's not based
# on a Sequel table.  I just needed a server construct.

class DEXServer
	attr_accessor :server # sequel model object, Server
	attr_accessor :units
	attr_accessor :rcon
	attr_accessor :timer
	attr_accessor :proc_tick
	
	def initialize(in_server, in_rcon, in_tick)
		@server = in_server
		@rcon = in_rcon
		@timer = setup_timer
		@proc_tick = in_tick
		@units = []
	end
	
	# closeup is called when we're going to remove this object
	# We won't rely on the GC to do this for us at this point.
	def closeup()
		@timer.cancel
		@rcon.close_connection
		@server.save_changes
	end
	
	def rcon_callback(packet)
		puts "DEBUG: per-server rcon_callback:"
		puts packet.packet.to_s[12..-1]
	end
	
	def setup_timer
		@timer = EM::PeriodicTimer.new(5) do
			puts "\nPeriodic server-specific event: (ID #{server.id}) #{Time.now} - #{rcon.error?}"
			if rcon.ready
				@server.last_tick = @proc_tick.to_i
				rcon.command("mqueue.gameinfo") do |packet|
					#puts "DEBUG: Processing gameinfo: #{packet.packet.to_s[12..-1].strip}"
					# get the current tick, we'll store it off later.
					begin
						gi_payload = JSON.parse(packet.packet.to_s[12..-1].strip)
						if gi_payload['seed'].to_i != server.seed
							server.seed = gi_payload['seed'].to_i
							server.last_tick = 0
						end
						if gi_payload['tick'].to_i < @proc_tick.to_i
							@units.each do |unit|
								# reset units back to a basic starting point.
								unit.restart
							end
							server.last_tick = 0
						end
						@proc_tick = gi_payload['tick'].to_i
					rescue JSON::ParserError
						puts "Unable to parse json content."
					end
				end
				@units.each do |unit|
					# run the various queries we'll process.
					unit.query_and_process
				end
			else
				puts "Server #{server.id} - Rcon is not ready to communicate (disconnected?)"
			end
		end
	end
end
