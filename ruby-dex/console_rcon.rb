require 'eventmachine'
require 'eventmachine-tail'
require "#{__dir__}/lib/em-rcon"
require "#{__dir__}/lib/params_from_yaml"
require 'json'
require 'yaml'

# DB model
require "#{__dir__}/model/init"

# modules that define what operations we handle:

dbsettings = Helper::Config.hash_from_yaml("#{__dir__}/config/db.yml", "#{__dir__}/config/defaults/db.yml")
settings = Helper::Config.hash_from_yaml("#{__dir__}/config/servers.yml", "#{__dir__}/config/defaults/servers.yml")

class KeyboardHandler < EM::Connection
	include EM::Protocols::LineText2

	def initialize(q, rcon)
		@queue = q
		@rcon = rcon
	end

	def receive_line(data)
		p data
		if data.strip == "quit"
			EM.stop
		end
		#@queue.push(data)
		@rcon.command(data) do |packet|
			puts "Actual output: "
			#puts packet.packet.to_s[0..11] # junk?
			res = packet.packet.to_s[12..-1].strip.split("\n")
			puts res
			res.each do |rr|
				begin
					jres = JSON.parse(rr)
					puts "#receive_lines, json data: #{jres}"
				rescue
					puts "Unable to process: #{rr}"
				end
			end
		end
	end
end

EM.run do
	rcon = nil
	settings[:servers].each do |ss|
		p ss
		# "connect" rcon ?
		rcon = EventMachine::connect ss[:ip], ss[:rcon_port], EMRcon, ss[:rcon_pass]
	end
	
	q = EM::Queue.new
	EM.open_keyboard(KeyboardHandler, q, rcon)
end
