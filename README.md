# RCON Data EXchange for Factorio

This is the FishBus gaming Data EXchange system designed for Factorio.

## General design concept

The goal behind this design was to interact with the Factorio game server in order to save & load data without having to deal with files local to the game server.  We wanted to store our user information in a database, and have it persist between maps.

In order to achieve this, an RCON-based design was built (Thanks Factorio 0.16.29!) to exchange messages between Factorio (mqueue) and a service written in Ruby (DEX).

DEX connects to the game server via RCON, and then polls every few seconds for new messages to the various defined systems (just `keystore` at the time of writing) using the mqueue script for Factorio.  Each system in DEX can then process it's queue, and respond as necessary.

### Keystore functionality

Using DEX with the keystore scripts, we can then store arbitrary key-value data.  The keystore script knows about different `modules` and thus anything which can be handled as a key-value pair, really gets tagged as module-key-value set, so that your "user permissions" module can have keys named the same as your "user stats" module, but the two won't conflict.

Additionally, the keystore scripts also know about server-specific and 'global' values.  I haven't come up with a use case for this yet, as my use case wants every server to share in the user permissions, but it's built-in.

Additional information about the Factorio scripts for the message queue & keystore can be found in factorio\_bits/README.md.

### RCON / DEX functionality

DEX is extensible by adding more mqueue\_unit derivatives, and registering them with the $unitManager.  They will automatically have their query\_and\_process method called on every iteration.  `keystore.rb` is an operational example of this.  It's important to note the distiction between rcon.command & rcon.message:

* rcon.command will expect a response, and a response must be given.
* rcon.message will not expect a response, and if a response occurs, it will confuse the program.

